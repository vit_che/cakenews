<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Image Entity
 *
 * @property int $id
 * @property string $name
 * @property string $type
 *
 * @property \App\Model\Entity\Article[] $articles
 */
class Image extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'type' => true,
        'articles' => true
    ];

    protected function _getFiletype()
    {
        return explode('/',$this->_properties['type'])[1];
    }

    protected function _getFilepath()
    {
       return env('IMAGES_PATH').'/'.$this->_properties['hashname'][0].'/'.$this->_properties['hashname'];
    }

    protected function _getPrefilepath()
    {
        return env('IMAGES_PREVIEW_PATH').'/'.$this->_properties['hashname'][0].'/'.$this->_properties['previewname'];
    }

    public function Prename($src)
    {
            $pre_image_name = 'pre_' . $this->_properties['hashname'];
            $pre_shortpath = env('IMAGES_PREVIEW_PATH') . '/' . $this->_properties['hashname'][0];
            $pre_fullpath = WWW_ROOT . $pre_shortpath . '/' . $pre_image_name;
            $pre_dirname = WWW_ROOT . $pre_shortpath;
            $this->_properties['previewname'] = $pre_image_name;
            if (!file_exists($pre_dirname)) {
                mkdir($pre_dirname);
                $source = $src;
                $height = 100; //параметр высоты превью
                $width = 100; //параметр ширины превью
                $rgb = 0xffffff; //цвет заливки несоответствия
                $size = getimagesize($source);//узнаем размеры картинки (дает нам масив size)
                $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1)); //определяем тип файла
                $icfunc = "imagecreatefrom" . $format;   //определение функции соответственно типу файла
                if (!function_exists($icfunc)) return false;  //если нет такой функции прекращаем работу скрипта
                $x_ratio = $width / $size[0]; //пропорция ширины будущего превью
                $y_ratio = $height / $size[1]; //пропорция высоты будущего превью
                $ratio = min($x_ratio, $y_ratio);
                $use_x_ratio = ($x_ratio == $ratio); //соотношения ширины к высоте
                $new_width = $use_x_ratio ? $width : floor($size[0] * $ratio); //ширина превью
                $new_height = !$use_x_ratio ? $height : floor($size[1] * $ratio); //высота превью
                $new_left = $use_x_ratio ? 0 : floor(($width - $new_width) / 2); //расхождение с заданными параметрами по ширине
                $new_top = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2); //расхождение с заданными параметрами по высоте
                $img = imagecreatetruecolor($width, $height); //создаем вспомогательное изображение пропорциональное превью
                imagefill($img, 0, 0, $rgb); //заливаем его…
                $photo = $icfunc($source); //достаем наш исходник
                imagecopyresampled($img, $photo, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]); //копируем на него нашу превью с учетом расхождений
                $handle = fopen($pre_fullpath, "w");
                imagejpeg($img, $pre_fullpath); //выводим результат (превью картинки)
                fclose($handle);
                imagedestroy($img); // Очищаем память после выполнения скрипта
                imagedestroy($photo);

                return $pre_image_name;
            }
        }
    }



