<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="/css/cakenews.css">

   
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
  </head>
  <body>

  <!--  -->
  <nav class="navbar navbar-toggleable-md navbar-light lightblue">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#"><img src="/img/newslogo.png" width="40%" height="40%"></a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto ">
        <li class="nav-item active">
          <a class="nav-link whitefont" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link whitefont" href="#">World News</a>
        </li>
        <li class="nav-item">
          <a class="nav-link whitefont" href="#">Ukranian News</a>
        </li>
        <li class="nav-item">
          <a class="nav-link whitefont" href="#">Local News</a>
        </li>

      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>

    <!-- carusel -->

<!--    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">   -->
 <!--     <div class="carousel-inner" role="listbox">                                           -->
 <!--       <div class="carousel-item active">                                              -->
<!--          <img class="d-block img-fluid" src="/image/news_6.jpg" alt="First slide">     -->
 <!--       </div>                                                                          -->
 <!--       <div class="carousel-item">                                                        -->
<!--          <img class="d-block img-fluid" src="/image/news_5.jpg" alt="Second slide">        -->
<!--        </div>                                                                              -->
<!--        <div class="carousel-item">                                                         -->
<!--          <img class="d-block img-fluid" src="/image/news_7.jpg" alt="Third slide">            -->
<!--        </div>                                                                          -->
<!--      </div>                                                                        -->
<!--      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">  -->
<!--        <span class="carousel-control-prev-icon" aria-hidden="true"></span>    -->
<!--        <span class="sr-only">Previous</span>                                  -->
<!--      </a>                                                                      -->
<!--      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">    -->
<!--        <span class="carousel-control-next-icon" aria-hidden="true"></span>   -->
<!--        <span class="sr-only">Next</span>                                       -->
<!--      </a>                                                           -->
<!--  </div>                                                            -->

  

<!--  -->

  <!-- <div class="contnews container-fluid mrgtop40" id="world">

    <h3> World news </h3>

    <div class="row">

      <div class="card col-md-4 pdtop15" style="width: 20rem;">
        <img class="card-img-top" src="image/news_1.jpeg" alt="Card image cap">
        <div class="card-block">
          <h4 class="card-title">News title</h4>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" class="btn btn-primary">Read more</a>
        </div>
      </div>

      <div class="card col-md-4 pdtop15" style="width: 20rem;">
        <img class="card-img-top" src="image/news_1.jpeg" alt="Card image cap">
        <div class="card-block">
          <h4 class="card-title">News title</h4>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" class="btn btn-primary">Read more</a>
        </div>
      </div>

      <div class="card col-md-4 pdtop15" style="width: 20rem;">
        <img class="card-img-top" src="image/news_1.jpeg" alt="Card image cap">
        <div class="card-block">
          <h4 class="card-title">News title</h4>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" class="btn btn-primary">Read more</a>
        </div>
      </div>

    </div>

  </div>
 -->

<!-- 
  <div class="contnews container-fluid mrgtop40" id="ukraine">

    <h3> Ukranian news </h3>

    <div class="row">

      <div class="card col-md-4 pdtop15" style="width: 20rem;">
        <img class="card-img-top" src="image/news_1.jpeg" alt="Card image cap">
        <div class="card-block">
         <h4 class="card-title">News title</h4>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" class="btn btn-primary">Read more</a>
        </div>
      </div>

      <div class="card col-md-4 pdtop15" style="width: 20rem;">
        <img class="card-img-top" src="image/news_1.jpeg" alt="Card image cap">
        <div class="card-block">
          <h4 class="card-title">News title</h4>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" class="btn btn-primary">Read more</a>
        </div>
      </div>

      <div class="card col-md-4 pdtop15" style="width: 20rem;">
        <img class="card-img-top" src="image/news_1.jpeg" alt="Card image cap">
        <div class="card-block">
          <h4 class="card-title">News title</h4>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" class="btn btn-primary">Read more</a>
        </div>
      </div>

    </div>

  </div> -->


<!-- <div class="contnews container-fluid mrgtop40" id="ukraine">

  <h3> Local news </h3>

  <div class="row">

    <div class="card col-md-4 pdtop15" style="width: 20rem;">
      <img class="card-img-top" src="image/news_1.jpeg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title">News title</h4>
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        <a href="#" class="btn btn-primary">Read more</a>
      </div>
    </div>

    <div class="card col-md-4 pdtop15" style="width: 20rem;">
      <img class="card-img-top" src="image/news_1.jpeg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title">News title</h4>
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        <a href="#" class="btn btn-primary">Read more</a>
      </div>
    </div>

    <div class="card col-md-4 pdtop15" style="width: 20rem;">
      <img class="card-img-top" src="image/news_1.jpeg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title">News title</h4>
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        <a href="#" class="btn btn-primary">Read more</a>
      </div>
    </div>

  </div>

</div> -->





 <?= $this->Flash->render() ?>

    <div class="container clearfix">
   
        <?= $this->fetch('content') ?>
        
              
    </div>






<div class="footer container-fluid pdn0">
  <nav class="navbar navbar-inverse bg-primary">
    footer
  </nav>


</div>






    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
  </body>
</html>