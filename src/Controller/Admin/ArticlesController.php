<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 *
 * @method \App\Model\Entity\Article[] paginate($object = null, array $settings = [])
 */
class ArticlesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    //        $this->viewBuilder()->setLayout('indexnews');

    public function home(){

        $this->paginate = [
            'contain' => ['Categories', 'Authors']
        ];
        $articles = $this->paginate($this->Articles);

        $this->set(compact('articles'));
        $this->set('_serialize', ['articles']);

       $this->viewBuilder()->setLayout('indexnews');

    }


    public function index()
    {
        $this->paginate = [
            'contain' => ['Categories', 'Authors']
        ];
        $articles = $this->paginate($this->Articles);

        $this->set(compact('articles'));
        $this->set('_serialize', ['articles']);

        $this->viewBuilder()->setLayout('indexnews');
    }

    /**
     * View method
     *
     * @param string|null $id Article id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => ['Categories', 'Authors', 'Images']
        ]);

        $this->set('article', $article);
        $this->set('_serialize', ['article']);

       $this->viewBuilder()->setLayout('indexnews');
        
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $article = $this->Articles->newEntity();
        $image = $this->Articles->Images->newEntity(); //

        if ($this->request->is('post')) {

            $article = $this->Articles->patchEntity($article, $this->request->getData());

            $data = $this->request->getData(); //
            $image = $this->Articles->Images->patchEntity($image, $this->request->getData()); //

            $imagePrename = pathinfo( $data['submittedfile']['name'])['filename']; //only filename
            $image->type = $data['submittedfile']['type']; //
            $image->name = $imagePrename.'.'.$image->filetype;  //File

            if ($this->Articles->save($article)) {

                $imageHashName = hash('md5', $imagePrename.$image->id); // Create HashName with id
                $image->hashname = $imageHashName.'.'.$image->filetype;  // File hashName in Folder
                $shortpath = env('IMAGES_PATH').'/'.$imageHashName[0]; //img/article_images/q
                $fullpath = WWW_ROOT.$shortpath.'/'.$imageHashName.'.'.$image->filetype; //
                $dirname = WWW_ROOT.$shortpath;   //

                if (!file_exists($dirname)) { //
                    mkdir($dirname); //
                }

                move_uploaded_file($data['submittedfile']['tmp_name'], $fullpath); //

                if ($data['preview'] == '1') {

                    $image->previewname = $image->Prename($fullpath);

                    if ($this->Articles->Images->save($image)) {    //Save

                        $this->Flash->success(__('The image with Preview has been saved.'));
                    } else {
                        $this->Flash->error(__('The image with Preview could not be saved. Please, try again.'));
                    }

                } elseif ($this->Articles->Images->save($image)) {    //Save


                 $this->Flash->success(__('This is ELSEIF!!!!!The image with HashName has been saved.'));

                } else {
                    $this->Flash->error(__('The image with HashName could not be saved. Please, try again.'));
                }

                $this->Flash->success(__('The image has been saved.'));

                $this->Flash->success(__('The article has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The article could not be saved. Please, try again.'));
        }

        $categories = $this->Articles->Categories->find('list', ['limit' => 200]);
        $authors = $this->Articles->Authors->find('list', ['limit' => 200]);
        $images = $this->Articles->Images->find('list', ['limit' => 200]);
        $this->set(compact('article', 'categories', 'authors', 'images'));
        $this->set('_serialize', ['article']);
    }
    /**
     * Edit method
     *
     * @param string|null $id Article id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => ['Images']
        ]);


        if ($this->request->is(['patch', 'post', 'put'])) {

            $article = $this->Articles->patchEntity($article, $this->request->getData());

            if ($this->Articles->save($article)) {
                $this->Flash->success(__('The article has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The article could not be saved. Please, try again.'));
        }
        $categories = $this->Articles->Categories->find('list', ['limit' => 200]);
        $authors = $this->Articles->Authors->find('list', ['limit' => 200]);
        $images = $this->Articles->Images->find('list', ['limit' => 200]);
        $this->set(compact('article', 'categories', 'authors', 'images'));
        $this->set('_serialize', ['article']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Article id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get($id);
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('The article has been deleted.'));
        } else {
            $this->Flash->error(__('The article could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
