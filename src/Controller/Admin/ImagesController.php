<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Http\ServerRequest;

/**
 * Images Controller
 *
 * @property \App\Model\Table\ImagesTable $Images
 *
 * @method \App\Model\Entity\Image[] paginate($object = null, array $settings = [])
 */
class ImagesController extends AppController
{
    public function index()
    {
        $images = $this->paginate($this->Images);

        $this->set(compact('images'));
        $this->set('_serialize', ['images']);
    }

    public function view($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => ['Articles']
        ]);

        $this->set('image', $image);
        $this->set('_serialize', ['image']);
    }


    public function add()
    {
        $image = $this->Images->newEntity();

        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $image = $this->Images->patchEntity($image, $this->request->getData());

            $imagePrename = explode('.', $data['submittedfile']['name'])[0]; //only filename
            $image->type = $data['submittedfile']['type'];
            $image->name = $imagePrename.'.'.$image->filetype;  //File Name   name.jpeg

            if ($this->Images->save($image)) {

                $imageHashName = hash('md5', $imagePrename.$image->id); // Create HashName with id
                $image->hashname = $imageHashName.'.'.$image->filetype;  // File hashName in Folder
                $shortpath = env('IMAGES_PATH').'/'.$imageHashName[0]; //img/article_images/q
                $fullpath = WWW_ROOT.$shortpath.'/'.$imageHashName.'.'.$image->filetype;
                $dirname = WWW_ROOT.$shortpath;

                if (!file_exists($dirname)) {
                    mkdir($dirname);
                }

                move_uploaded_file($data['submittedfile']['tmp_name'], $fullpath);

                if ($data['preview'] == '1') {

                $image->previewname = $image->Prename($fullpath);
                    if ($this->Images->save($image)) {    //Save
                            $this->Flash->success(__('The image with Preview has been saved.'));
                        } else {
                            $this->Flash->error(__('The image with Preview could not be saved. Please, try again.'));
                        }
                    }

                if ($this->Images->save($image)) {    //Save
                    $this->Flash->success(__('The image with HashName has been saved.'));
                } else {
                    $this->Flash->error(__('The image wwith HashName could not be saved. Please, try again.'));
                }

                $this->Flash->success(__('The image has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The image could not be saved. Please, try again.'));
        }
        $articles = $this->Images->Articles->find('list', ['limit' => 200]);
        $this->set(compact('image', 'articles'));
        $this->set('_serialize', ['image']);
    }
    /**
     * Edit method
     *
     * @param string|null $id Image id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => ['Articles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $image = $this->Images->patchEntity($image, $this->request->getData());
            
            if ($this->Images->save($image)) {
                $this->Flash->success(__('The image has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The image could not be saved. Please, try again.'));
        }
        $articles = $this->Images->Articles->find('list', ['limit' => 200]);
        $this->set(compact('image', 'articles'));
        $this->set('_serialize', ['image']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Image id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $image = $this->Images->get($id);
        $del_file_path = WWW_ROOT.$image->filepath; //new Path
        $del_prefile_path = WWW_ROOT.$image->prefilepath; //new Path
        $del_dir = WWW_ROOT.env('IMAGES_PATH').'/'.$image->hashname[0];
        $del_dir_pre = WWW_ROOT.env('IMAGES_PREVIEW_PATH').'/'.$image->hashname[0];

        if ($this->Images->delete($image)) {

            if (isset($del_file_path)) {   //Delete Main image
                unlink($del_file_path);
            } else {
                $this->Flash->error(__('There is not '.$image->name.' in Directory'));
            }
            if (count(scandir($del_dir)) <= 2) {   // delete Empty Image  Dir
                rmdir($del_dir);
            }

            if (isset($del_prefile_path)) {  //delete preview Image
                unlink($del_prefile_path);
            }
            if (count(scandir($del_dir_pre)) <= 2) {   // delete Empty Preview  Dir
                rmdir($del_dir_pre);
            }

            $this->Flash->success(__('The image has been deleted.'));
        } else {
            $this->Flash->error(__('The image could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
